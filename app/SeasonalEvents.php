<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeasonalEvents extends Model
{
    public function events()
    {
        return $this->belongsTo('App\Events', 'event_id');


    }

    public function imagesSingle()
    {
        return $this->belongsToMany('App\images', 'seasonal_events_images_pivot','seasonal_event_id', 'image_id')->withTimestamps();
    }

    public function imagesCarousel()
    {
        return $this->belongsToMany('App\images', 'seasonal_events_carousel_pivot','seasonal_event_id', 'image_id')->withTimestamps();
    }

    public function SeasonalBanner()
    {
        return $this->hasMany('App\SeasonalEventsBanner', 'seasonal_events_id');
    }

    public function PDF()
    {
        return $this->belongsToMany('App\PDF', 'seasonals_pdf_pivot','seasonal_event_id', 'pdf_id')->withTimestamps();
    }
}
