<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsOnBanner extends Model
{
    public function WhatsOnContent()
    {
        return $this->belongsTo('App\WhatsOnContent', 'whats_on_contents_id');

    }

    public function BannerMedia()
    {
        return $this->belongsToMany('App\Media', 'whatson_banner_media_pivot','whats_on_banners_id', 'media_id')->withTimestamps();
    }
}
