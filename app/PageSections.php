<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSections extends Model
{
    public function imagesSingle()
    {
        return $this->belongsToMany('App\images', 'page_sections_images_pivot','page_section_id', 'image_id')->withTimestamps();
    }

    public function imagesCarousel()
    {
        return $this->belongsToMany('App\images', 'page_sections_carousel_pivot','page_section_id', 'image_id')->withTimestamps();
    }

    public function PDF()
    {
        return $this->belongsToMany('App\PDF', 'pagesections_pdf_pivot','page_section_id', 'pdf_id')->withTimestamps();
    }

    public function pages()
    {
        return $this->belongsTo('App\pages', 'page_id');
    }

     public function PageBanner()
    {
        return $this->hasMany('App\PageSectionsBanner', 'section_id');
    }
}
