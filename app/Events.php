<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    public function imagesSingle()
    {
        return $this->belongsToMany('App\images', 'event_images_pivot','event_id', 'image_id')->withTimestamps();
    }

    public function imagesCarousel()
    {
        return $this->belongsToMany('App\images', 'event_carousel_pivot','event_id', 'image_id')->withTimestamps();
    }

    public function SeasonalEvent()
    {
        return $this->hasMany('App\SeasonalEvents');
    }

    public function WhatsOnContent()
    {
        return $this->hasMany('App\WhatsOnContent', 'id', 'event_id');
    }

    public function EventsBanner()
    {
        return $this->hasMany('App\EventsBanner', 'events_id');
    }
}
