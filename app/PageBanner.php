<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageBanner extends Model
{
    public function Pages()
    {
        return $this->belongsTo('App\pages', 'pages_id');

    }

    public function BannerMedia()
    {
        return $this->belongsToMany('App\Media', 'page_banner_media_pivot','page_banners_id', 'media_id')->withTimestamps();
    }
}


