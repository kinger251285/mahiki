<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use VanOns\Laraberg\Models\Gutenbergable;

class pages extends Model
{
    use Gutenbergable;

    public function imagesSingle()
    {
        return $this->belongsToMany('App\images', 'pages_header_images_pivot','page_id', 'image_id')->withTimestamps();
    }

    public function imagesCarousel()
    {
        return $this->belongsToMany('App\images', 'pages_header_images_carousel_pivot','page_id', 'image_id')->withTimestamps();
    }

    public function sections()
    {
        return $this->hasMany('App\PageSections', 'page_id');
    }

    public function PageBanner()
    {
        return $this->hasMany('App\PagesBanner', 'pages_id');
    }
}
