<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsOnContent extends Model
{
    public function imagesSingle()
    {
        return $this->belongsToMany('App\images', 'whatsoncontent_images_pivot','whats_on_content_id', 'image_id')->withTimestamps();
    }

    public function imagesCarousel()
    {
        return $this->belongsToMany('App\images', 'whatsoncontent_carousel_pivot','whats_on_content_id', 'image_id')->withTimestamps();
    }

    public function events()
    {
        return $this->belongsTo('App\Events', 'event_id');
    }

    public function WhatsOnBanner()
    {
        return $this->hasMany('App\WhatsOnContent', 'whats_on_contents_id');
    }
}
