<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeasonalEventsBanner extends Model
{
    public function SeasonalEvents()
    {
        return $this->belongsTo('App\SeasonalEvents', 'seasonal_events_id');

    }

    public function BannerMedia()
    {
        return $this->belongsToMany('App\Media', 'seasonal_events_banners_media','seasonal_events_banners_id', 'media_id')->withTimestamps();
    }
}
