<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSectionsBanner extends Model
{
    public function BannerMedia()
    {
        return $this->belongsToMany('App\Media', 'sections_banners_media_pivot', 'sections_banners_id', 'media_id')->withTimestamps();
    }

    public function PageSections()
    {
        return $this->belongsTo('App\PageSections', 'section_id');

    }
}
