<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUsContent extends Model
{
    public function imagesSingle()
    {
        return $this->belongsToMany('App\images', 'contact_us_content_images_pivot','contact_us_id', 'image_id')->withTimestamps();
    }

    public function imagesCarousel()
    {
        return $this->belongsToMany('App\images', 'contact_us_content_carousel_pivot','contact_us_id', 'image_id')->withTimestamps();
    }
}
