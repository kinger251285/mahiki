<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SeasonalEventsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $defaultData = parent::toArray($request);

        $Single = false;
        $Carousel = false;

        if($this->event_media == 'single'){
            $Single = true;
        }

        if($this->event_media == 'carousel'){
            $Carousel = true;
        }

        $additionalData = [

            'single' => $Single,
            'carousel' => $Carousel,
            

        ];

        return array_merge($defaultData, $additionalData);
    }
}
