<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierImagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $defaultData = parent::toArray($request);     
        

        $additionalData = [
             
            'supplier_logo' => $this->whenPivotLoaded('logo_suppliers', function () {
            return $this->pivot->photo;
        }),

            'supplier_images' => $this->whenPivotLoaded('images_suppliers_pivot', function () {
            return $this->pivot->photo;
        }),
            

        ];


        

       

        return array_merge($defaultData, $additionalData);
    }
}
