<?php

namespace App\Http\Controllers;

use App\ContactUsContent;
use App\images;
use Illuminate\Http\Request;
use App\Http\Resources\ContactUsContentResource;
use Illuminate\Support\Collection;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class ContactUsContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ContactUsContents = ContactUsContent::first()->get();

        $related = new Collection();

        foreach($ContactUsContents as $Content){            

        if($Content->media_type == 'single'){
            $SingleImageContent = ContactUsContent::with('imagesSingle')->get();
            $related = $related->merge($SingleImageContent);
        }elseif($Content->media_type == 'carousel'){
            $CarouselImageContent = ContactUsContent::with('imagesCarousel')->get();
            $related = $related->merge($CarouselImageContent);
        }else{
            $NoMediaContent = ContactUsContent::first()->get();
            $related = $related->merge($NoMediaContent);
        }

        }

        return ContactUsContentResource::collection($related);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactUsContent  $contactUsContent
     * @return \Illuminate\Http\Response
     */
    public function show(ContactUsContent $contactUsContent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactUsContent  $contactUsContent
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactUsContent $contactUsContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactUsContent  $contactUsContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ContactContent = ContactUsContent::findOrFail($id);

        $Title = $request->get('contact_title');
        $Excerpt = $request->get('contact_excerpt');
        $AddressTitle = $request->get('address_title');
        $Street = $request->get('street_address');
        $Address2 = $request->get('address_2');
        $Town = $request->get('town');  
        $City = $request->get('city');
        $County = $request->get('county');                
        $Postcode = $request->get('postcode');
        $Longitude = $request->get('longitude');
        $Latitude = $request->get('latitude');
        $PhoneTitle = $request->get('phone_title');
        $PhoneNo = $request->get('phone_no');
        $EmailTitle = $request->get('email_title');
        $Email = $request->get('email_address');         

        $ContactContent->contact_title = $Title;
        $ContactContent->contact_excerpt = $Excerpt;
        $ContactContent->address_title = $AddressTitle;
        $ContactContent->street_address = $Street;
        $ContactContent->address_2 = $Address2;
        $ContactContent->town = $Town;  
        $ContactContent->city = $City;
        $ContactContent->county = $County;                
        $ContactContent->postcode = $Postcode;
        $ContactContent->longitude = $Longitude;
        $ContactContent->latitude = $Latitude;
        $ContactContent->phone_title = $PhoneTitle;
        $ContactContent->phone_no = $PhoneNo;
        $ContactContent->email_title = $EmailTitle;
        $ContactContent->email = $Email; 

        $ContactContent->save();

        return response()->json('Contact Details Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactUsContent  $contactUsContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactUsContent $contactUsContent)
    {
        //
    }

    public function AddContactsSingleImage(Request $request)
    {
        $ContactTitle = $request->get('contact_title');
        $ContactExcerpt = $request->get('contact_excerpt');
        $AddressTitle = $request->get('address_title');
        $StreetAddress = $request->get('street_address');
        $Address2 = $request->get('address_2');
        $Town = $request->get('town');
        $City = $request->get('city');
        $County = $request->get('county');  
        $Postcode = $request->get('postcode');
        $Longitude = $request->get('longitude');
        $Latitude = $request->get('latitude');
        $PhoneTitle = $request->get('phone_title');
        $PhoneNo = $request->get('phone_no');
        $EmailTitle = $request->get('email_title');
        $EmailAddress = $request->get('email_address');

        $Contact = array (
                'contact_title' => $ContactTitle,
                'contact_excerpt' => $ContactExcerpt,
                'address_title' => $AddressTitle,
                'street_address' => $StreetAddress, 
                'address_2' => $Address2,
                'town' => $Town,  
                'city' => $City,
                'county' => $County, 
                'postcode' => $Postcode,
                'longitude' => $Longitude, 
                'latitude' => $Latitude, 
                'phone_title' => $PhoneTitle, 
                'phone_no' => $PhoneNo, 
                'email_title' => $EmailTitle, 
                'email' => $EmailAddress,                                                  
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        ContactUsContent::insert($Contact);        

        $ContactId = DB::getPdo()->lastInsertId();  

        $Contact = ContactUsContent::findOrFail($ContactId);      

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/Pages/ContactUs/SingleImage', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/Pages/ContactUs/SingleImage', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $Contact->imagesSingle()->attach($ImageId);
            
            $Contact->media_type = $MediaType;

            $Contact->save();

        return response()->json('Successfully Added Single Image Section');

    }

    public function AddContactsCarouselImages(Request $request)
    {

        $ContactTitle = $request->get('contact_title');
        $ContactExcerpt = $request->get('contact_excerpt');
        $AddressTitle = $request->get('address_title');
        $StreetAddress = $request->get('street_address');
        $Address2 = $request->get('address_2');
        $Town = $request->get('town');
        $City = $request->get('city');
        $County = $request->get('county');  
        $Postcode = $request->get('postcode');
        $Longitude = $request->get('longitude');
        $Latitude = $request->get('latitude');
        $PhoneTitle = $request->get('phone_title');
        $PhoneNo = $request->get('phone_no');
        $EmailTitle = $request->get('email_title');
        $EmailAddress = $request->get('email_address');

        $Contact = array (
                'contact_title' => $ContactTitle,
                'contact_excerpt' => $ContactExcerpt,
                'address_title' => $AddressTitle,
                'street_address' => $StreetAddress, 
                'address_2' => $Address2,
                'town' => $Town,  
                'city' => $City,
                'county' => $County, 
                'postcode' => $Postcode,
                'longitude' => $Longitude, 
                'latitude' => $Latitude, 
                'phone_title' => $PhoneTitle, 
                'phone_no' => $PhoneNo, 
                'email_title' => $EmailTitle, 
                'email' => $EmailAddress,                                                  
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        ContactUsContent::insert($Contact);        

        $ContactId = DB::getPdo()->lastInsertId();  

        $Contact = ContactUsContent::findOrFail($ContactId);         

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Pages/ContactUs/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Pages/ContactUs/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/ContactUs/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Pages/ContactUs/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/ContactUs/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Pages/ContactUs/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/ContactUs/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Pages/ContactUs/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/ContactUs/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Pages/ContactUs/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/ContactUs/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Pages/ContactUs/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $Contact->imagesCarousel()->attach($ImageId);
            $i++;
            }            
                       
            $Contact->media_type = $MediaType;

            $Contact->save();
        

        return response()->json('Successfully Added Carousel Images');
        
    }
}

