<?php

namespace App\Http\Controllers;

use App\images;
use App\pages;
use App\PDF;
use App\PageSections;
use Illuminate\Http\Request;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function AddSingleHeader(Request $request, $slug)
    {
        $Page = pages::where('slug', $slug)->first();

        $PageName = $Page->page_name;

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/PageMedia/'.$PageName.'', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/PageMedia/'.$PageName.'', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $Page->imagesSingle()->attach($ImageId);
            
            $Page->header_media = $MediaType;

            $Page->save();

        return response()->json('Successfully Added Single Image Header');
    }


    public function AddSectionSingleImage(Request $request, $slug)
    {
        $SectionTitle = $request->get('section_title');
        $SectionText = $request->get('section_text');
        $SectionSlug = $request->get('section_slug');
        $SectionOrder = $request->get('section_order');

        $PageId = $request->get('page_id'); 

        $PageName = pages::findOrFail($PageId)->value('page_name');     

        $Section = array (
                'section_title' => $SectionTitle,
                'section_text' => $SectionText,                
                'slug' => $SectionSlug, 
                'section_order' => $SectionOrder, 
                'page_id' => $PageId,                                                         
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        PageSections::insert($Section);

        $SectionId = DB::getPdo()->lastInsertId();  

        $PageSections = PageSections::findOrFail($SectionId); 

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $PageSections->imagesSingle()->attach($ImageId);
            
            $PageSections->section_media = $MediaType;

            $PageSections->save();



        return response()->json('Successfully Added Single Image Section');
    }

    public function EditSectionSingleImage(Request $request, $slug)
    {
        $SectionTitle = $request->get('section_title');
        $SectionId = $request->get('section_id');       
        
        $PageId = $request->get('page_id'); 

        $PageName = pages::findOrFail($PageId)->value('page_name');          

        $PageSections = PageSections::findOrFail($SectionId);      

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $PageSections->imagesSingle()->attach($ImageId);
            
            $PageSections->section_media = $MediaType;

            $PageSections->save();

        return response()->json('Successfully Added Single Image Section');
    }


    public function AddCarouselImages(Request $request, $slug)
    {
        $Page = pages::where('slug', $slug)->first();

        $PageName = $Page->page_name;

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Pages/'.$PageName.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Pages/'.$PageName.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $Page->imagesCarousel()->attach($ImageId);
            $i++;
            }

            $Page->header_media = $MediaType;

            $Page->save();
        

        return response()->json('Successfully Added Carousel Images');
    }

    public function AddSectionCarouselImages(Request $request, $slug)
    {
        $SectionTitle = $request->get('section_title');
        $SectionText = $request->get('section_text');
        $SectionSlug = $request->get('section_slug');
        $SectionOrder = $request->get('section_order');
        $PageId = $request->get('page_id'); 

        $PageName = pages::findOrFail($PageId)->value('page_name');     

        $Section = array (
                'section_title' => $SectionTitle,
                'section_text' => $SectionText,                
                'slug' => $SectionSlug, 
                'section_order' => $SectionOrder, 
                'page_id' => $PageId,                                                         
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        PageSections::insert($Section);

        $SectionId = DB::getPdo()->lastInsertId();  

        $PageSections = PageSections::findOrFail($SectionId);

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $PageSections->imagesCarousel()->attach($ImageId);
            $i++;
            }
            
            
            $PageSections->section_media = $MediaType;

            $PageSections->save();
        

        return response()->json('Successfully Added Carousel Images');
    }

    public function EditSectionCarouselImages(Request $request, $slug)
    {
        $SectionTitle = $request->get('section_title');
        $SectionId = $request->get('section_id');
        
        $PageId = $request->get('page_id'); 

        $PageName = pages::findOrFail($PageId)->value('page_name');          

        $PageSections = PageSections::findOrFail($SectionId);

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Pages/'.$PageName.'/PageSection/'.$SectionTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $PageSections->imagesCarousel()->attach($ImageId);
            $i++;
            }
            
            
            $PageSections->section_media = $MediaType;

            $PageSections->save();
        

        return response()->json('Successfully Added Carousel Images');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\images  $images
     * @return \Illuminate\Http\Response
     */
    public function show(images $images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\images  $images
     * @return \Illuminate\Http\Response
     */
    public function edit(images $images)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\images  $images
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, images $images)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\images  $images
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $Slug = $request->get('slug');
        $Supplier = Suppliers::where('slug', $Slug)->first();

        $Image = images::findOrFail($id);            
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Supplier->images()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }

    public function deleteSingleImage(Request $request, $id)
    {
        $Slug = $request->get('slug');
        
        $Page = pages::where('slug', $Slug)->first();

        $Image = images::findOrFail($id);            
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Page->imagesSingle()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }

    public function deleteCarouselImage(Request $request, $id)
    {
        $Slug = $request->get('slug');
        
        $Page = pages::where('slug', $Slug)->first();

        $Image = images::findOrFail($id);            
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Page->imagesCarousel()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }

    public function deleteSectionSingleImage(Request $request, $id) 
    {
        $SectionId = $request->get('SectionId');
        
        $PageSection = PageSections::where('id', $SectionId)->first();

        $Image = images::findOrFail($id);            
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $PageSection->imagesSingle()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }

    public function deleteSectionCarouselImage(Request $request, $id)
    {
        $SectionId = $request->get('SectionId');
        
        $PageSection = PageSections::where('id', $SectionId)->first();

        $Image = images::findOrFail($id);             
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $PageSection->imagesCarousel()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }
}
