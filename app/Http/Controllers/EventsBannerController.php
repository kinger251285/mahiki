<?php

namespace App\Http\Controllers;

use App\EventsBanner;
use App\Media;
use App\Events;
use Illuminate\Http\Request;
use App\Http\Resources\EventBannerResource;
use Carbon\carbon;
use DB;

class EventsBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function GetEventsBanner($id)
    {               

        $EventBanner = EventsBanner::where('events_id', $id)->with('BannerMedia')->get();

         return EventBannerResource::collection($EventBanner);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $EventID = $request->get('events_id');
        $BannerType = $request->get('banner_type');
        $Edited = $request->get('isEdited');        

        if($Edited == 'yes'){

            $EventBannersID = EventsBanner::where('events_id', $EventID)->value('id');

            $EventBanners = EventsBanner::findOrFail($EventBannersID);

            $EventBanners->banner_type = $BannerType;

            $EventBanners->save();

        }else {

            $Data = array (
                'events_id' => $EventID,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            EventsBanner::insert($Data);

            $EventBannersID = DB::getPdo()->lastInsertId();

        }        

        $EventBanners = EventsBanner::findOrFail($EventBannersID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $EventBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added An Event Banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventsBanner  $eventsBanner
     * @return \Illuminate\Http\Response
     */
    public function show(EventsBanner $eventsBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventsBanner  $eventsBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(EventsBanner $eventsBanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventsBanner  $eventsBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventsBanner $eventsBanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventsBanner  $eventsBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventsBanner $eventsBanner)
    {
        //
    }
}
