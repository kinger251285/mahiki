<?php

namespace App\Http\Controllers;

use App\PageSectionsBanner;
use Illuminate\Http\Request;
use App\Media;
use App\Http\Resources\PageSectionsBannerResource;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class PageSectionsBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }


    public function GetThisBanner($id)
    {
        $SectionBanner = PageSectionsBanner::where('section_id', $id)->with('BannerMedia')->get();

                
         return PageSectionsBannerResource::collection($SectionBanner);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $SectionID = $request->get('section_id');
        $BannerType = $request->get('banner_type');
        $Edited = $request->get('isEdited');   

        if($Edited == 'yes'){      

        $SectionBannerID = PageSectionsBanner::where('section_id', $SectionID)->value('id');

        $SectionBanners = PageSectionsBanner::findOrFail($SectionBannerID);

        $SectionBanners->banner_type = $BannerType;

        $SectionBanners->save();

        }else {

            $Data = array (
                'section_id' => $SectionID,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            PageSectionsBanner::insert($Data);

            $SectionBannerID = DB::getPdo()->lastInsertId();
        }
        
        $SectionBanners = PageSectionsBanner::findOrFail($SectionBannerID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $SectionBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added An Event Banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageSectionsBanner  $pageSectionsBanner
     * @return \Illuminate\Http\Response
     */
    public function show(PageSectionsBanner $pageSectionsBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageSectionsBanner  $pageSectionsBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(PageSectionsBanner $pageSectionsBanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageSectionsBanner  $pageSectionsBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageSectionsBanner $pageSectionsBanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageSectionsBanner  $pageSectionsBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageSectionsBanner $pageSectionsBanner)
    {
        //
    }
}
