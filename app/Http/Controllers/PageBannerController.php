<?php

namespace App\Http\Controllers;

use App\PageBanner;
use App\pages;
use App\Media;
use Illuminate\Http\Request;
use App\Http\Resources\PageBannerResource;
use Carbon\carbon;
use DB;


class PageBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $PageId = pages::where('slug', $slug)->value('id');

        $PageBanner - PageBanner::where('pages_id', $PageId)->with('BannerMedia')->get();

         return PageBannerResource::collection($PageBanner);
        
    }

     public function GetPagesBanner($slug)
    {       

        $PageId = pages::where('slug', $slug)->value('id');

        $PageBanner = PageBanner::where('pages_id', $PageId)->with('BannerMedia')->get();

         return PageBannerResource::collection($PageBanner);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PageID = $request->get('page_id');
        $BannerType = $request->get('banner_type');
        $Edited = $request->get('isEdited');        

        if($Edited == 'yes'){

            $PageBannersID = PageBanner::where('pages_id', $PageID)->value('id');

            $PageBanners = PageBanner::findOrFail($PageBannersID);

            $PageBanners->banner_type = $BannerType;

            $PageBanners->save();

        }else {

            $Data = array (
                'pages_id' => $PageID,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            PageBanner::insert($Data);

            $PageBannersID = DB::getPdo()->lastInsertId();

        }        

        $PageBanners = PageBanner::findOrFail($PageBannersID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $PageBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added A Page Banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageBanner  $pageBanner
     * @return \Illuminate\Http\Response
     */
    public function show(PageBanner $pageBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageBanner  $pageBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(PageBanner $pageBanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageBanner  $pageBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageBanner $pageBanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageBanner  $pageBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageBanner $pageBanner)
    {
        //
    }
}
