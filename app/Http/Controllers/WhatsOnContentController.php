<?php

namespace App\Http\Controllers;

use App\WhatsOnContent;
use App\Events;
use App\images;
use Illuminate\Http\Request;
use App\Http\Resources\EventsContentResource;
use Illuminate\Support\Collection;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class WhatsOnContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {    
        $EventId = Events::where('event_slug', $slug)->value('id');

        $EventContents = WhatsOnContent::where('event_id', $EventId)->get();

        $related = new Collection();

        foreach($EventContents as $Event){

        if($Event->event_media == 'single'){
            $SingleImageEvent = WhatsOnContent::with('imagesSingle')->where('id', $Event->id)->get();
            $related = $related->merge($SingleImageEvent);
        }elseif($Event->event_media == 'carousel'){
            $CarouselImageEvent = WhatsOnContent::with('imagesCarousel')->where('id', $Event->id)->get();
            $related = $related->merge($CarouselImageEvent);
        }else{
            $NoMediaEvent = WhatsOnContent::where('id', $Event->id)->get();
            $related = $related->merge($NoMediaEvent);
        }

        }

        return EventsContentResource::collection($related);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WhatsOnContent  $whatsOnContent
     * @return \Illuminate\Http\Response
     */
    public function show(WhatsOnContent $whatsOnContent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WhatsOnContent  $whatsOnContent
     * @return \Illuminate\Http\Response
     */
    public function edit(WhatsOnContent $whatsOnContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WhatsOnContent  $whatsOnContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WhatsOnContent $whatsOnContent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WhatsOnContent  $whatsOnContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhatsOnContent $whatsOnContent)
    {
        //
    }

     public function AddEventSingleImage(Request $request)
    {

        $ContentExcerpt = $request->get('excerpt');
        $EventId = $request->get('event_id');          

        $Event = array (
                'excerpt' => $ContentExcerpt,
                'event_id' => $EventId,                 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),               
            ); 

        WhatsOnContent::insert($Event);

        $WhatsOnContentId = DB::getPdo()->lastInsertId();  

        $WhatsOnContent = WhatsOnContent::findOrFail($WhatsOnContentId);

        $EventTitle = Events::where('id', $EventId)->value('event_title');       

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/Events/'.$EventTitle.'Content/SingleImage', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/Events/'.$EventTitle.'Content/SingleImage', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $WhatsOnContent->imagesSingle()->attach($ImageId);
            
            $WhatsOnContent->event_media = $MediaType;

            $WhatsOnContent->save();

        return response()->json('Successfully Added Single Image Section');

    }

    public function AddEventCarouselImages(Request $request)
    {

        $ContentExcerpt = $request->get('excerpt');
        $EventId = $request->get('event_id');          

        $Event = array (
                'excerpt' => $ContentExcerpt,
                'event_id' => $EventId,                 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),               
            ); 

        WhatsOnContent::insert($Event);

        $WhatsOnContentId = DB::getPdo()->lastInsertId();  

        $WhatsOnContent = WhatsOnContent::findOrFail($WhatsOnContentId);

        $EventTitle = Events::where('id', $EventId)->value('event_title');          

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $WhatsOnContent->imagesCarousel()->attach($ImageId);
            $i++;
            }            
            
            $WhatsOnContent->event_media = $MediaType;

            $WhatsOnContent->save();
        

        return response()->json('Successfully Added Carousel Images');
        
    }
}
