<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;

class MailchimpController extends Controller
{
    public function store(Request $request)
    {
    	$LastName = $request->get('last_name');
    	$Email = $request->get('email');
    	$Name = $request->get('name');

    	if($LastName == '') {
    	Newsletter::subscribeOrUpdate($Email, ['FNAME'=>$Name]);
    	} else {
    		return response()->json('Thank You For Subscribing');

    	}

    }
}
