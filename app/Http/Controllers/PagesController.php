<?php

namespace App\Http\Controllers;

use App\pages;
use Illuminate\Http\Request;
use App\Http\Resources\PagesResource;
use Carbon\carbon;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PagesResource::collection(pages::all()); 
    }

    public function GetPage($slug)
    {
        $Page = pages::where('slug', $slug)->get(); 

        return PagesResource::collection($Page);
    }

    public function GetPageHeaderImages($slug)
    {
        $Page = pages::where('slug', $slug)->get();

        $HeaderMedia = pages::where('slug', $slug)->value('header_media');        

        if($HeaderMedia == 'single'){
                $Page = pages::with('imagesSingle')->where('slug', $slug)->get();
            }

        if($HeaderMedia == 'carousel'){
                $Page = pages::with('imagesCarousel')->where('slug', $slug)->get();
            }
        

        return PagesResource::collection($Page);
    }

    public function GetFullPage($slug)
    {
        $Page = pages::where('slug', $slug)->with('sections.PageBanner.BannerMedia', 'sections.PDF')->get();

        return PagesResource::collection($Page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PageName = $request->get('page_name');
        $PageTitle = $request->get('page_title');
        $PageSubtitle = $request->get('page_subtitle');
        $Slug = $request->get('slug');
        $Excerpt = $request->get('excerpt');
        $TemplateId = $request->get('template');
        $Published = $request->get('is_active');

        if($Published == null){
            $Published = 0;
        }

        $Page = array (
                'page_name' => $PageName,
                'page_title' => $PageTitle,
                'page_subtitle' => $PageSubtitle,
                'slug' => $Slug, 
                'excerpt' => $Excerpt, 
                'template_id' => $TemplateId, 
                'is_active' => $Published,                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        pages::insert($Page);

        return response()->json('Successfully Added Page');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function show(pages $pages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function edit(pages $pages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PageName = $request->get('page_name');
        $PageTitle = $request->get('page_title');
        $PageSubtitle = $request->get('page_subtitle');
        $Slug = $request->get('slug');
        $Excerpt = $request->get('excerpt');
        $TemplateId = $request->get('template_id');
        $IsActive = $request->get('is_active');

        $Page = pages::findOrFail($id);

        $Page->page_name = $PageName;
        $Page->page_title = $PageTitle;
        $Page->page_subtitle = $PageSubtitle;
        $Page->slug = $Slug;
        $Page->excerpt = $Excerpt;
        $Page->template_id = $TemplateId;
        $Page->is_active = $IsActive;

        $Page->save();

        return response()->json('Successfully Edited');        
    }

    public function SubmitHeaderChange(Request $request, $id)
    {
        $slug = $request->get('slug');

        $Page = pages::findOrFail($id);

        $Single = $request->get('single');

        $Carousel = $request->get('carousel');

        if($Single !== '' && $Single == 'true'){
            $HeaderMedia = 'single';
        }

        if($Single !== '' && $Single == 'false'){
            $HeaderMedia = 'carousel';
        }

        if($Carousel !== '' && $Carousel == 'true'){
            $HeaderMedia = 'carousel';
        }

        if($Carousel !== '' && $Carousel == 'false'){
            $HeaderMedia = 'single';
        }

        $Page->header_media = $HeaderMedia;

        $Page->save();

        return response()->json('Successfully Edited');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function destroy(pages $pages)
    {
        //
    }
}
