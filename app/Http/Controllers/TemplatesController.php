<?php

namespace App\Http\Controllers;

use App\templates;
use Illuminate\Http\Request;
use App\Http\Resources\TemplateResource;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class TemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TemplateResource::collection(templates::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $TemplateName = $request->get('name'); 


        if ($request->hasFile('preview')){

            $filename = str_replace(' ', '',$request->File('preview')->getClientOriginalName());

            /////// Main Image Compress /////

            $request->File('preview')->storeAs('public/images/templates/'.$TemplateName.'', $filename);       

            $PreviewImg = $request->File('preview')->storeAs('storage/images/templates/'.$TemplateName.'', $filename);

            $PhotoMain = Image::make($PreviewImg)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $PhotoMain->save($PreviewImg);
        }

        if ($request->hasFile('wireframe')){

            $filename = str_replace(' ', '',$request->File('wireframe')->getClientOriginalName());

            /////// Main Image Compress /////

            $request->File('wireframe')->storeAs('public/images/templates/'.$TemplateName.'', $filename);       

            $WireframeImg = $request->File('wireframe')->storeAs('storage/images/templates/'.$TemplateName.'', $filename);           

            $PhotoMain = Image::make($WireframeImg)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $PhotoMain->save($WireframeImg);
        }

        if ($request->hasFile('template_file')){

            $template_filename = str_replace(' ', '',$request->File('template_file')->getClientOriginalName());

            /////// Main Image Compress /////

            $request->File('template_file')->storeAs('public/images/templates/layout/'.$TemplateName.'', $template_filename);       

            $TemplateFile = $request->File('template_file')->storeAs('storage/images/templates/layout/'.$TemplateName.'', $template_filename); 

            
        }

            ////////////////////////////////            

            $file = array (
                'template_name' => $TemplateName,
                'template_preview' => $PreviewImg,
                'template_wireframe' => $WireframeImg,
                'template_file' => $TemplateFile,
                'template_route' => $template_filename,                                            
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            );             

            templates::insert($file);            
        

        return response()->json('Successfully Added Template');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function show(templates $templates)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function edit(templates $templates)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, templates $templates)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\templates  $templates
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $Template = templates::findOrFail($id);                   
        
        storage::delete($Template->template_preview);
        storage::delete($Template->template_wireframe);
        storage::delete($Template->template_file);

        $Template->delete();         

        return response()->json('Successfully Deleted');
    }
}
