<?php

namespace App\Http\Controllers;

use App\Events;
use App\images;
use App\SeasonalEvents;
use App\Media;
use App\EventsBanner;
use Illuminate\Http\Request;
use App\Http\Resources\EventsResource;
use Illuminate\Support\Collection;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EventsResource::collection(Events::all());
    }

    public function GetEvent($id) 
    {
        $Events = Events::where('id', $id)->get();

        $related = new Collection();

        foreach($Events as $Event){

        if($Event->event_media == 'single'){
            $SingleImageEvent = Events::with('imagesSingle')->where('id', $Event->id)->get();
            $related = $related->merge($SingleImageEvent);
        }elseif($Event->event_media == 'carousel'){
            $CarouselImageEvent = Events::with('imagesCarousel')->where('id', $Event->id)->get();
            $related = $related->merge($CarouselImageEvent);
        }else{
            $NoMediaEvent = Events::where('id', $Event->id)->get();
            $related = $related->merge($NoMediaEvent);
        }

        }

        return EventsResource::collection($related);
    }

    public function GetEventContent($slug)
    {
        $Events = Events::where('event_slug', $slug)->with('EventsBanner.BannerMedia')->get();
         

        return EventsResource::collection($Events);
    }

    public function EventSummary()
    {
        $Events = Events::orderby('event_day')->with('EventsBanner.BannerMedia')->get();         

        return EventsResource::collection($Events);           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $EventTitle = $request->get('event_title');
        $EventExcerpt = $request->get('event_excerpt');
        $EventSlug = $request->get('event_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');         
        $Promoter = $request->get('promoter');
        $createdBy = $request->get('created_by');      

             

        $Event = array (
                'event_title' => $EventTitle,
                'event_excerpt' => $EventExcerpt,
                'event_slug' => $EventSlug,
                'event_day' => $EventDay,
                'event_date' => $EventDate,  
                'start_time' => $EventStart,
                'end_time' => $EventEnd,                         
                'created_by' => $createdBy, 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),               
            ); 

        Events::insert($Event);

        $EventID = DB::getPdo()->lastInsertId();


        $BannerType = $request->get('banner_type');
               

        

            $Data = array (
                'events_id' => $EventID,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            EventsBanner::insert($Data);

            $EventBannersID = DB::getPdo()->lastInsertId();

            

        $EventBanners = EventsBanner::findOrFail($EventBannersID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $EventBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added An Event Banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function show(Events $events)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function edit(Events $events)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Event = Events::findOrFail($id);

        $EventTitle = $request->get('event_title');
        $EventExcerpt = $request->get('event_excerpt');
        $EventSlug = $request->get('event_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');         
        
        $createdBy = $request->get('created_by'); 
        

        $Event->event_title = $EventTitle;
        $Event->event_excerpt = $EventExcerpt;
        $Event->event_slug = $EventSlug;
        $Event->event_day = $EventDay;
        $Event->event_date = $EventDate;
        $Event->start_time = $EventStart;
        $Event->end_time = $EventEnd;
        
        $Event->created_by = $createdBy;        

        $Event->save();

        return response()->json('Successfully Updated');

    }

    public function SubmitMediaChange(Request $request, $id)
    {
        
        $Event = Events::findOrFail($id);

        $Single = $request->get('single');

        $Carousel = $request->get('carousel');

        if($Single !== '' && $Single == 'true'){
            $Media = 'single';
        }

        if($Single !== '' && $Single == 'false'){
            $Media = 'carousel';
        }

        if($Carousel !== '' && $Carousel == 'true'){
            $Media = 'carousel';
        }

        if($Carousel !== '' && $Carousel == 'false'){
            $Media = 'single';
        }

        $Event->event_media = $Media;

        $Event->save();

        return response()->json('Successfully Edited');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Events  $events
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $EventMedia = Events::where('id', $id)->value('event_media');
        $Events = Events::find($id);

        if($EventMedia == 'single'){

        $Events->imagesSingle()->detach();

        $ImagesSingle = $Events->imagesSingle;

            foreach($ImagesSingle as $Image){

                
                storage::delete($Image->photo);            
                $Image->delete(); 

            }    
        

        }elseif($EventMedia == 'carousel'){

        $Events->imagesCarousel()->detach(); 

        $ImagesCarousel = $Events->imagesCarousel;

            foreach($ImagesCarousel as $Image){                

                
                storage::delete($Image->photo); 
                storage::delete($Image->photo300px);
                storage::delete($Image->photo400px);
                storage::delete($Image->photo600px);
                storage::delete($Image->photo800px);
                storage::delete($Image->photo1000px);           
                $Image->delete(); 

            }

                   

        }

        $Events->delete();         

        return response()->json('Successfully Deleted');
    }

    public function DeleteSingleImage(Request $request, $id) 
    {
        $EventId = $request->get('EventId');
        
        $Event = Events::where('id', $EventId)->first();

        $Image = images::findOrFail($id);            
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Event->imagesSingle()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }




    public function DeleteCarouselImage(Request $request, $id)
    {
        $EventId = $request->get('EventId');
        
        $Event = Events::where('id', $EventId)->first();

        $Image = images::findOrFail($id);             
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Event->imagesCarousel()->detach($id);

        $Image->delete();         

        return response()->json('Successfully Deleted');
    }


    public function AddEventSingleImage(Request $request)
    {

        $EventTitle = $request->get('event_title');
        $EventExcerpt = $request->get('event_excerpt');
        $EventSlug = $request->get('event_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');         
        $Promoter = $request->get('promoter');
        $createdBy = $request->get('created_by');      

             

        $Event = array (
                'event_title' => $EventTitle,
                'event_excerpt' => $EventExcerpt,
                'event_slug' => $EventSlug,
                'event_day' => $EventDay,
                'event_date' => $EventDate,  
                'start_time' => $EventStart,
                'end_time' => $EventEnd,                         
                'created_by' => $createdBy, 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),               
            ); 

        Events::insert($Event);

        $EventId = DB::getPdo()->lastInsertId();  

        $Events = Events::findOrFail($EventId);      

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/Events/'.$EventTitle.'/SingleImage', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/Events/'.$EventTitle.'/SingleImage', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $Events->imagesSingle()->attach($ImageId);
            
            $Events->event_media = $MediaType;

            $Events->save();

        return response()->json('Successfully Added Single Image Section');

    }

    public function AddEventCarouselImages(Request $request)
    {

        $EventTitle = $request->get('event_title');
        $EventExcerpt = $request->get('event_excerpt');
        $EventSlug = $request->get('event_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');         
        $createdBy = $request->get('created_by');     

             

        $Event = array (
                'event_title' => $EventTitle,
                'event_excerpt' => $EventExcerpt,
                'event_slug' => $EventSlug,
                'event_day' => $EventDay,
                'event_date' => $EventDate,  
                'start_time' => $EventStart,
                'end_time' => $EventEnd,                               
                'created_by' => $createdBy, 
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),               
            ); 

        Events::insert($Event);

        $EventId = DB::getPdo()->lastInsertId();  

        $Events = Events::findOrFail($EventId);         

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $Events->imagesCarousel()->attach($ImageId);
            $i++;
            }
            
            
            $Events->event_media = $MediaType;

            $Events->save();
        

        return response()->json('Successfully Added Carousel Images');
        
    }




    public function EditEventSingleImage(Request $request)
    {

        $EventId = $request->get('event_id');         

        $Events = Events::findOrFail($EventId); 

        $EventTitle = Events::where('id', $EventId)->value('event_title');      

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/Events/'.$EventTitle.'/SingleImage', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/Events/'.$EventTitle.'/SingleImage', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $Events->imagesSingle()->attach($ImageId);
            
            $Events->event_media = $MediaType;

            $Events->save();

        return response()->json('Successfully Added Single Image Section');

    }

    public function EditEventCarouselImages(Request $request)
    {

        $EventId = $request->get('event_id');         

        $Events = Events::findOrFail($EventId); 

        $EventTitle = Events::where('id', $EventId)->value('event_title');         

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/Events/'.$EventTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/Events/'.$EventTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $Events->imagesCarousel()->attach($ImageId);
            $i++;
            }
            
            
            $Events->event_media = $MediaType;

            $Events->save();
        

        return response()->json('Successfully Added Carousel Images');
        
    }
}
