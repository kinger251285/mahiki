<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\BookingFormEmail;
use App\Mail\PartyFormEmail;

class EmailController extends Controller
{
    public function SendBookingEmail(Request $request)
    {
        $LastName = $request->get('last_name');
        $Email = $request->get('email');
        $Name = $request->get('name');
        $Phone = $request->get('phone');
        $EventType = $request->get('event_type');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');
        $StartTime = $request->get('start_time');
        $Guests = $request->get('no_guests');
        $Additional = $request->get('additional');

        $data = [
            'name'=> $Name, 
            'email'=> $Email, 
            'phone'=> $Phone,
            'event_type'=> $EventType,
            'event_day' => $EventDay,
            'event_date' => $EventDate,
            'start_time' => $StartTime,
            'no_guests' => $Guests,
            'additional' => $Additional,
        ];

        $Emails = [$Email, 'reservations@mahiki.com','david@bitterlemoncreative.co.uk', 'aaron@mahiki.com'];

        if($LastName == '') {
            foreach($Emails as $Email){
        Mail::to($Email)->send(new BookingFormEmail($data));
        }
        return response()->json('Booking Email Sent');
        } else {
            return response()->json('Booking Email Sent, Thanks');

        }		

    }

    public function SendPartyEmail(Request $request)
    {
        $LastName = $request->get('last_name');
        $Email = $request->get('email');
        $Name = $request->get('name');
        $Phone = $request->get('phone');
        $EventType = $request->get('event_type');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');
        $StartTime = $request->get('timings');
        $Guests = $request->get('no_guests');
        $Additional = $request->get('additional');
        $Budget = $request->get('budget');

        $data = [
            'name'=> $Name, 
            'email'=> $Email, 
            'phone'=> $Phone,
            'event_type'=> $EventType,
            'event_day' => $EventDay,
            'event_date' => $EventDate,
            'start_time' => $StartTime,
            'no_guests' => $Guests,
            'additional' => $Additional,
            'budget' => $Budget,
        ];

        $Emails = [$Email, 'reservations@mahiki.com','david@bitterlemoncreative.co.uk', 'aaron@mahiki.com'];

        if($LastName == '') {
            foreach($Emails as $Email){
        Mail::to($Email)->send(new PartyFormEmail($data));
            }
        return response()->json('Party Email Sent');
        } else {
                    return response()->json('Party Email Sent, Thanks');
        }
        

    }
}
