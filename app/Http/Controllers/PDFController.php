<?php

namespace App\Http\Controllers;

use App\PDF;
use App\PageSections;
use App\SeasonalEvents;
use Illuminate\Http\Request;
use App\Http\Resources\PDFResource;
use Illuminate\Support\Collection;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;


class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PDFResource::collection(PDF::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $SectionId = $request->get('section_id');

        $PageSections = PageSections::findOrFail($SectionId);                 

        $PDFS = $request->all();

        $PDFS = $PDFS['pdf'];
        
        $i = 0;

        foreach($PDFS as $PDF){                      

            $file = array (                
                'PDF_file' => $PDF,                                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            );             

            PDF::insert($file);           

            $PDFId = DB::getPdo()->lastInsertId();

            $PageSections->PDF()->attach($PDFId);

            $i++;

            }            
            
            $PageSections->pdf = 'true';

            $PageSections->save();
        

        return response()->json('Successfully Added PDF');
    }

    public function SeasonalStore(Request $request)
    {

        $SeasonalId = $request->get('seasonal_id');

        $Seasonals = SeasonalEvents::findOrFail($SeasonalId);                 

        $PDFS = $request->all();

        $PDFS = $PDFS['pdf'];
        
        $i = 0;

        foreach($PDFS as $PDF){                      

            $file = array (                
                'PDF_file' => $PDF,                                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            );             

            PDF::insert($file);           

            $PDFId = DB::getPdo()->lastInsertId();

            $Seasonals->PDF()->attach($PDFId);

            $i++;

            }  
        

        return response()->json('Successfully Added PDF');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function show(PDF $pDF)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function edit(PDF $pDF)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PDF $pDF)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PDF  $pDF
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) 
    {
                
        $PDF = PDF::findOrFail($id);       

        $PDF->pageSectionsPDF()->detach();                       

        PDF::destroy($id);

        return response()->json('Successfully Deleted PDF'); 
    }

    public function SeasonalDelete(Request $request, $id) 
    {
                
        $PDF = PDF::findOrFail($id);       

        $PDF->seasonalsPDF()->detach();                       

        PDF::destroy($id);

        return response()->json('Successfully Deleted PDF'); 
    }
}
