<?php

namespace App\Http\Controllers;

use App\SeasonalEventsBanner;
use App\Media;
use Illuminate\Http\Request;
use App\Http\Resources\SeasonalEventBannerResource;
use Carbon\carbon;
use DB;

class SeasonalEventsBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function GetEventsBanner($id)
    {               

        $EventBanner = SeasonalEventsBanner::where('seasonal_events_id', $id)->with('BannerMedia')->get();

         return SeasonalEventBannerResource::collection($EventBanner);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $EventID = $request->get('seasonal_events_id');
        $BannerType = $request->get('banner_type');
        $Edited = $request->get('isEdited');        

        if($Edited == 'yes'){

            $EventBannersID = SeasonalEventsBanner::where('seasonal_events_id', $EventID)->value('id');

            $EventBanners = SeasonalEventsBanner::findOrFail($EventBannersID);

            $EventBanners->banner_type = $BannerType;

            $EventBanners->save();

        }else {

            $Data = array (
                'seasonal_events_id' => $EventID,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            SeasonalEventsBanner::insert($Data);

            $EventBannersID = DB::getPdo()->lastInsertId();

        }        

        $EventBanners = SeasonalEventsBanner::findOrFail($EventBannersID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $EventBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added A Seasonal Event Banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SeasonalEventsBanner  $seasonalEventsBanner
     * @return \Illuminate\Http\Response
     */
    public function show(SeasonalEventsBanner $seasonalEventsBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SeasonalEventsBanner  $seasonalEventsBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(SeasonalEventsBanner $seasonalEventsBanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SeasonalEventsBanner  $seasonalEventsBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeasonalEventsBanner $seasonalEventsBanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SeasonalEventsBanner  $seasonalEventsBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(SeasonalEventsBanner $seasonalEventsBanner)
    {
        //
    }
}
