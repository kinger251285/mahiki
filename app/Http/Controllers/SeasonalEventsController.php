<?php

namespace App\Http\Controllers;

use App\SeasonalEvents;
use App\SeasonalEventsBanner;
use App\Media;
use App\images;
use Illuminate\Http\Request;
use App\Http\Resources\SeasonalEventsResource;
use Carbon\carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class SeasonalEventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SeasonalEventsResource::collection(SeasonalEvents::all()); 
    }

    public function GetSeasonal($id) 
    {
        $Seasonals = SeasonalEvents::where('id', $id)->with('SeasonalBanner.BannerMedia')->get();

        return SeasonalEventsResource::collection($Seasonals);
    }

    public function GetSeasonalPDF($id) 
    {
        $Seasonals = SeasonalEvents::where('id', $id)->with('PDF')->with('SeasonalBanner.BannerMedia')->get();

        return SeasonalEventsResource::collection($Seasonals);
    }

    public function SeasonalSummary()
    {
        $Today = Carbon::now();

        $Events = SeasonalEvents::where('display_from', '<', $Today)->where('display_until', '>', $Today)->with('PDF')->with('SeasonalBanner.BannerMedia')->get();

        return SeasonalEventsResource::collection($Events);
    }

    public function SeasonalDisplay($slug)
    {
        $Seasonal = SeasonalEvents::where('seasonal_slug', $slug)->with('SeasonalBanner.BannerMedia')->get();       


        return SeasonalEventsResource::collection($Seasonal);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $SeasonalTitle = $request->get('seasonal_title');
        $SeasonalExcerpt = $request->get('seasonal_excerpt');
        $FromDate = $request->get('from-date');
        $ToDate = $request->get('to-date');
        $EventId = $request->get('event_id');
        $SeasonalSlug = $request->get('seasonal_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');
        $createdBy = $request->get('created_by');

        $Event = array (
                'seasonal_title' => $SeasonalTitle,
                'seasonal_excerpt' => $SeasonalExcerpt,
                'display_from' => $FromDate,
                'display_until' => $ToDate, 
                'event_day' => $EventDay,
                'event_date' => $EventDate,  
                'start_time' => $EventStart,
                'end_time' => $EventEnd, 
                'created_by' => $createdBy,
                'seasonal_slug' => $SeasonalSlug,                                                       
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        SeasonalEvents::insert($Event);        

        $EventId = DB::getPdo()->lastInsertId();  

        $BannerType = $request->get('banner_type');       

            $Data = array (
                'seasonal_events_id' => $EventId,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            SeasonalEventsBanner::insert($Data);

            $EventBannersID = DB::getPdo()->lastInsertId();            

        $EventBanners = SeasonalEventsBanner::findOrFail($EventBannersID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $EventBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added An Event Banner');        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SeasonalEvents  $seasonalEvents
     * @return \Illuminate\Http\Response
     */
    public function show(SeasonalEvents $seasonalEvents)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SeasonalEvents  $seasonalEvents
     * @return \Illuminate\Http\Response
     */
    public function edit(SeasonalEvents $seasonalEvents)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SeasonalEvents  $seasonalEvents
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $SeasonalEvents = SeasonalEvents::findOrFail($id);

        $SeasonalTitle = $request->get('seasonal_title');
        $SeasonalExcerpt = $request->get('seasonal_excerpt');
        $EventDate = $request->get('event_date');
        $FromDate = $request->get('from_date');
        $ToDate = $request->get('to_date');
        $EventDay = $request->get('event_day');        
        $SeasonalSlug = $request->get('seasonal_slug');
        $StartTime = $request->get('start_time');
        $EndTime = $request->get('end_time');   

        
        $SeasonalEvents->seasonal_title = $SeasonalTitle;
        $SeasonalEvents->seasonal_excerpt = $SeasonalExcerpt;
        $SeasonalEvents->event_date = $EventDate;
        $SeasonalEvents->display_from = $FromDate;
        $SeasonalEvents->display_until = $ToDate;
        $SeasonalEvents->event_day = $EventDay;        
        $SeasonalEvents->seasonal_slug = $SeasonalSlug;
        $SeasonalEvents->start_time = $StartTime;
        $SeasonalEvents->end_time = $EndTime;


        $SeasonalEvents->save();

        return response()->json('Successfully Updated Seasonal Event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SeasonalEvents  $seasonalEvents
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $SeasonalEvent = SeasonalEvents::findOrFail($id);  
        $SeasonalEvent->delete();

        return response()->json('Successfully Deleted');
    }

    public function DeleteSingleImage(Request $request, $id) 
    {
        $EventId = $request->get('EventId');
        
        $Event = SeasonalEvents::where('id', $EventId)->first();

        $Image = images::findOrFail($id);            
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Event->imagesSingle()->detach($id);
        $Image->delete();         

        return response()->json('Successfully Deleted');
    }




    public function DeleteCarouselImage(Request $request, $id)
    {
        $EventId = $request->get('EventId');
        
        $Event = SeasonalEvents::where('id', $EventId)->first();

        $Image = images::findOrFail($id);             
        
        storage::delete($Image->photo);
        storage::delete($Image->photo300px);
        storage::delete($Image->photo400px);
        storage::delete($Image->photo600px);
        storage::delete($Image->photo800px);
        storage::delete($Image->photo1000px);

        $Event->imagesCarousel()->detach($id);

        $Image->delete();         

        return response()->json('Successfully Deleted');
    }

    public function AddSeasonalSingleImage(Request $request)
    {
        $SeasonalTitle = $request->get('seasonal_title');
        $SeasonalExcerpt = $request->get('seasonal_excerpt');
        $FromDate = $request->get('from-date');
        $ToDate = $request->get('to-date');
        $EventId = $request->get('event_id');
        $SeasonalSlug = $request->get('seasonal_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');
        $createdBy = $request->get('created_by');

        $Event = array (
                'seasonal_title' => $SeasonalTitle,
                'seasonal_excerpt' => $SeasonalExcerpt,
                'display_from' => $FromDate,
                'display_until' => $ToDate, 
                'event_day' => $EventDay,
                'event_date' => $EventDate,  
                'start_time' => $EventStart,
                'end_time' => $EventEnd, 
                'created_by' => $createdBy,
                'seasonal_slug' => $SeasonalSlug,                                                       
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        SeasonalEvents::insert($Event);        

        $EventId = DB::getPdo()->lastInsertId();  

        $Events = SeasonalEvents::findOrFail($EventId);      

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/SingleImage', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/SingleImage', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $Events->imagesSingle()->attach($ImageId);
            
            $Events->event_media = $MediaType;

            $Events->save();

        return response()->json('Successfully Added Single Image Section');

    }

    public function AddSeasonalCarouselImages(Request $request)
    {

        $SeasonalTitle = $request->get('seasonal_title');
        $SeasonalExcerpt = $request->get('seasonal_excerpt');
        $FromDate = $request->get('from-date');
        $ToDate = $request->get('to-date');
        $EventId = $request->get('event_id');
        $SeasonalSlug = $request->get('seasonal_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $EventStart = $request->get('start_time');
        $EventEnd = $request->get('end_time');
        $createdBy = $request->get('created_by');

        $Event = array (
                'seasonal_title' => $SeasonalTitle,
                'seasonal_excerpt' => $SeasonalExcerpt,
                'display_from' => $FromDate,
                'display_until' => $ToDate, 
                'event_day' => $EventDay,
                'event_date' => $EventDate,  
                'start_time' => $EventStart,
                'end_time' => $EventEnd, 
                'created_by' => $createdBy,
                'seasonal_slug' => $SeasonalSlug,                                                       
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        SeasonalEvents::insert($Event);        

        $EventId = DB::getPdo()->lastInsertId();  

        $Events = SeasonalEvents::findOrFail($EventId);         

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $Events->imagesCarousel()->attach($ImageId);
            $i++;
            }
            
            
            $Events->event_media = $MediaType;

            $Events->save();
        

        return response()->json('Successfully Added Carousel Images');
        
    }

    public function EditSeasonalSingleImage(Request $request, $id)
    {
        $SeasonalEvents = SeasonalEvents::findOrFail($id); 

        
        $SeasonalTitle = $request->get('seasonal_title');
        $SeasonalExcerpt = $request->get('seasonal_excerpt');
        $FromDate = $request->get('from_date');
        $ToDate = $request->get('to_date');
        $EventId = $request->get('event_id');
        $SeasonalSlug = $request->get('seasonal_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $StartTime = $request->get('start_time');
        $EndTime = $request->get('end_time');
        $createdBy = $request->get('created_by');

        $SeasonalEvents->seasonal_title = $SeasonalTitle;
        $SeasonalEvents->seasonal_excerpt = $SeasonalExcerpt;
        $SeasonalEvents->event_date = $EventDate;
        $SeasonalEvents->display_from = $FromDate;
        $SeasonalEvents->display_until = $ToDate;
        $SeasonalEvents->event_day = $EventDay;        
        $SeasonalEvents->seasonal_slug = $SeasonalSlug;
        $SeasonalEvents->start_time = $StartTime;
        $SeasonalEvents->end_time = $EndTime;


        $SeasonalEvents->save();              

        $MediaType = 'single';

        if ($request->hasFile('photo')){

            $filename = str_replace(' ', '',$request->File('photo')->getClientOriginalName());

            $Title = $request->get('title');            

            /////// Main Image Compress /////

            $request->File('photo')->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/SingleImage', $filename);       

            $Img = $request->File('photo')->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/SingleImage', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////            

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => null,
                'photo400px' => null,
                'photo600px' => null,
                'photo800px' => null,
                'photo1000px' => null,
            );             

            images::insert($file); 
            
            }

            $ImageId = DB::getPdo()->lastInsertId();

            $SeasonalEvents->imagesSingle()->attach($ImageId);
            
            $SeasonalEvents->event_media = $MediaType;

            $SeasonalEvents->save();

        return response()->json('Successfully Added Single Image Section');

    }

    public function EditSeasonalCarouselImages(Request $request, $id)
    {

        $SeasonalEvents = SeasonalEvents::findOrFail($id); 

        $SeasonalTitle = $request->get('seasonal_title');
        $SeasonalExcerpt = $request->get('seasonal_excerpt');
        $FromDate = $request->get('from_date');
        $ToDate = $request->get('to_date');
        $EventId = $request->get('event_id');
        $SeasonalSlug = $request->get('seasonal_slug');
        $EventDay = $request->get('event_day');
        $EventDate = $request->get('event_date');  
        $StartTime = $request->get('start_time');
        $EndTime = $request->get('end_time');
        $createdBy = $request->get('created_by');

        $SeasonalEvents->seasonal_title = $SeasonalTitle;
        $SeasonalEvents->seasonal_excerpt = $SeasonalExcerpt;
        $SeasonalEvents->event_date = $EventDate;
        $SeasonalEvents->display_from = $FromDate;
        $SeasonalEvents->display_until = $ToDate;
        $SeasonalEvents->event_day = $EventDay;        
        $SeasonalEvents->seasonal_slug = $SeasonalSlug;
        $SeasonalEvents->start_time = $StartTime;
        $SeasonalEvents->end_time = $EndTime;


        $SeasonalEvents->save();          

        $MediaType = 'carousel';   

        $Photos = $request->all();

        $Titles = $Photos['title'];        

        $Photos = $Photos['photo'];
        
        $i = 0;

        foreach($Photos as $Photo){

            $filename = str_replace(' ', '',$Photo->getClientOriginalName());

            $Title = $Titles[$i];            

            /////// Main Image Compress /////

            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel', $filename);       

            $Img = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel', $filename);

            $PhotoMain = Image::make($Img);

            $PhotoMain->save($Img, 60);

            ////////////////////////////////

            /////// 300px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/300px', $filename);

            $Img300 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/300px', $filename);

            $img = Image::make($Img300)->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img300);
            

            //////////////////////////////////

            /////// 400px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/400px', $filename);

            $Img400 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/400px', $filename);

            $img = Image::make($Img400)->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img400);

            //////////////////////////////////

            /////// 600px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/600px', $filename);

            $Img600 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/600px', $filename);

            $img = Image::make($Img600)->resize(600, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img600);

            //////////////////////////////////

            /////// 800px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/800px', $filename);

            $Img800 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/800px', $filename);

            $img = Image::make($Img800)->resize(800, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img800);

            //////////////////////////////////

            /////// 1000px Image Re-Size & Compress /////
            $Photo->storeAs('public/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/1000px', $filename);

            $Img1000 = $Photo->storeAs('storage/images/SeasonalEvents/'.$SeasonalTitle.'/Carousel/1000px', $filename);

            $img = Image::make($Img1000)->resize(1000, null, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save($Img1000);

            //////////////////////////////////           

            $filesize = $PhotoMain->filesize();

            $file = array (
                'title' => $Title,
                'photo' => $Img,
                'size' => $filesize,                             
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'photo300px' => $Img300,
                'photo400px' => $Img400,
                'photo600px' => $Img600,
                'photo800px' => $Img800,
                'photo1000px' => $Img1000,
            );             

            images::insert($file);           

            $ImageId = DB::getPdo()->lastInsertId();

            $SeasonalEvents->imagesCarousel()->attach($ImageId);
            $i++;
            }
            
            
            $SeasonalEvents->event_media = $MediaType;

            $SeasonalEvents->save();
        

        return response()->json('Successfully Added Carousel Images');
        
    }
}
