<?php

namespace App\Http\Controllers;

use App\EventRooms;
use Illuminate\Http\Request;

class EventRoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventRooms  $eventRooms
     * @return \Illuminate\Http\Response
     */
    public function show(EventRooms $eventRooms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventRooms  $eventRooms
     * @return \Illuminate\Http\Response
     */
    public function edit(EventRooms $eventRooms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventRooms  $eventRooms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventRooms $eventRooms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventRooms  $eventRooms
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventRooms $eventRooms)
    {
        //
    }
}
