<?php

namespace App\Http\Controllers;

use App\PageSections;
use App\pages;
use App\images;
use App\PageSectionsBanner;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Resources\PageSectionsResource;
use Carbon\carbon;
use Illuminate\Support\Facades\Storage;
use DB;
use Image;

class PageSectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $PagesId = pages::where('slug', $slug)->value('id');

        $PageSections = PageSections::where('page_id', $PagesId)->get();  

        $related = new Collection();

        foreach($PageSections as $Section){

        if($Section->section_media == 'single'){
            $SingleImageSections = PageSections::with('imagesSingle')->where('id', $Section->id)->get();
            $related = $related->merge($SingleImageSections);
        }elseif($Section->section_media == 'carousel'){
            $CarouselImageSections = PageSections::with('imagesCarousel')->where('id', $Section->id)->get();
            $related = $related->merge($CarouselImageSections);
        }else{
            $NoMediaSections = PageSections::where('id', $Section->id)->get();
            $related = $related->merge($NoMediaSections);
        }

        }

        

        return PageSectionsResource::collection($related);
    }

    public function GetSection($slug, $id)
    {       


        $PagesId = pages::where('slug', $slug)->value('id');

        $PageSections = PageSections::where('page_id', $PagesId)->where('id', $id)->with('PDF')->get();  

        

        

        return PageSectionsResource::collection($PageSections);
    }


    public function GetFullPageSections($slug)
    {       


        $PagesId = pages::where('slug', $slug)->value('id');

        $PageSections = PageSections::where('page_id', $PagesId)->orderBy('section_order', 'asc')->get();  

        $related = new Collection();

        foreach($PageSections as $Section){

        if($Section->section_media == 'single'){
            $SingleImageSections = PageSections::with('imagesSingle')->with('PDF')->where('id', $Section->id)->get();
            $related = $related->merge($SingleImageSections);
        }elseif($Section->section_media == 'carousel'){
            $CarouselImageSections = PageSections::with('imagesCarousel')->with('PDF')->where('id', $Section->id)->get();
            $related = $related->merge($CarouselImageSections);
        }else{
            $NoMediaSections = PageSections::where('id', $Section->id)->with('PDF')->get();
            $related = $related->merge($NoMediaSections);
        }

        }

        

        return PageSectionsResource::collection($related);
    }


    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $SectionTitle = $request->get('section_title');
        $SectionText = $request->get('section_text');
        $SectionSlug = $request->get('section_slug');
        $SectionOrder = $request->get('section_order');
        $PageId = $request->get('page_id');   


        $Section = array (
                'section_title' => $SectionTitle,
                'section_text' => $SectionText,                
                'slug' => $SectionSlug, 
                'section_order' => $SectionOrder, 
                'page_id' => $PageId,                                                         
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

        PageSections::insert($Section);

        $SectionID = DB::getPdo()->lastInsertId();


        $BannerType = $request->get('banner_type');
               

        

            $Data = array (
                'section_id' => $SectionID,
                'banner_type' => $BannerType,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            PageSectionsBanner::insert($Data);

            $SectionBannersID = DB::getPdo()->lastInsertId();

            

        $SectionBanners = PageSectionsBanner::findOrFail($SectionBannersID);


        $i = 0;

        $Info = $request->all();

        $Medias = $Info['media'];

        $Title = $Info['title'];

        foreach($Medias as $Media){

            $Data = array (
                'title' => $Title[$i],
                'media' => $Media,                                                                                          
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),                
            ); 

            Media::insert($Data);

            $MediaID = DB::getPdo()->lastInsertId();

            $SectionBanners->BannerMedia()->attach($MediaID);

            $i++;

        }

        return response()->json('Successfully Added New Page Section');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageSections  $pageSections
     * @return \Illuminate\Http\Response
     */
    public function show(PageSections $pageSections)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageSections  $pageSections
     * @return \Illuminate\Http\Response
     */
    public function edit(PageSections $pageSections)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageSections  $pageSections
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PageSection = PageSections::findOrFail($id);

        $SectionTitle = $request->get('section_title');
        $SectionText = $request->get('section_text');
        $SectionSlug = $request->get('section_slug');
        $SectionOrder = $request->get('section_order');
        $PageId = $request->get('page_id');        

        $PageSection->section_title = $SectionTitle;
        $PageSection->section_text = $SectionText;
        $PageSection->slug = $SectionSlug;
        $PageSection->section_order = $SectionOrder;
        $PageSection->page_id = $PageId;

        $PageSection->save();       

        return response()->json('Successfully Editted Page');
    }

    public function SubmitMediaChange(Request $request, $id)
    {
        
        $PageSections = PageSections::findOrFail($id);

        $Single = $request->get('single');

        $Carousel = $request->get('carousel');

        if($Single !== '' && $Single == 'true'){
            $Media = 'single';
        }

        if($Single !== '' && $Single == 'false'){
            $Media = 'carousel';
        }

        if($Carousel !== '' && $Carousel == 'true'){
            $Media = 'carousel';
        }

        if($Carousel !== '' && $Carousel == 'false'){
            $Media = 'single';
        }

        $PageSections->section_media = $Media;

        $PageSections->save();

        return response()->json('Successfully Edited');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageSections  $pageSections
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $SectionMedia = PageSections::where('id', $id)->value('section_media');
        $PageSections = PageSections::find($id);

        if($SectionMedia == 'single'){

        $PageSections->imagesSingle()->detach();

        $ImagesSingle = $PageSections->imagesSingle;

            foreach($ImagesSingle as $Image){

                
                storage::delete($Image->photo);            
                $Image->delete(); 

            }    
        

        }elseif($SectionMedia == 'carousel'){

        $PageSections->imagesCarousel()->detach(); 

        $ImagesCarousel = $PageSections->imagesCarousel;

            foreach($ImagesCarousel as $Image){                

                
                storage::delete($Image->photo); 
                storage::delete($Image->photo300px);
                storage::delete($Image->photo400px);
                storage::delete($Image->photo600px);
                storage::delete($Image->photo800px);
                storage::delete($Image->photo1000px);           
                $Image->delete(); 

            }

                   

        }

        $PageSections->PDF()->detach();

        $PageSections->delete();         

        return response()->json('Successfully Deleted');
    }
}
