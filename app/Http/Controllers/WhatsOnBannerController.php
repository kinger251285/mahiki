<?php

namespace App\Http\Controllers;

use App\WhatsOnBanner;
use Illuminate\Http\Request;

class WhatsOnBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WhatsOnBanner  $whatsOnBanner
     * @return \Illuminate\Http\Response
     */
    public function show(WhatsOnBanner $whatsOnBanner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WhatsOnBanner  $whatsOnBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(WhatsOnBanner $whatsOnBanner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WhatsOnBanner  $whatsOnBanner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WhatsOnBanner $whatsOnBanner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WhatsOnBanner  $whatsOnBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhatsOnBanner $whatsOnBanner)
    {
        //
    }
}
