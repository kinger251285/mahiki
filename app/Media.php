<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public function EventsBanner()
    {
        return $this->belongsToMany('App\EventsBanner', 'events_banners_media_pivot', 'media_id', 'events_banners_id')->withTimestamps();
    }

    public function PagesBanner()
    {
        return $this->belongsToMany('App\PageBanner', 'page_banner_media_pivot', 'media_id', 'page_banners_id')->withTimestamps();
    }

    public function WhatsOnBanner()
    {
        return $this->belongsToMany('App\WhatsOnBanner', 'whatson_banner_media_pivot', 'media_id', 'whats_on_banners_id')->withTimestamps();
    }

    public function SeasonalEventsBanner()
    {
        return $this->belongsToMany('App\SeasonalEventsBanner', 'seasonal_events_banners_media', 'media_id', 'seasonal_events_banners_id')->withTimestamps();
    }

    public function PageSectionsBanner()
    {
        return $this->belongsToMany('App\PageSectionsBanner', 'sections_banners_media_pivot', 'media_id', 'sections_banners_id')->withTimestamps();
    }
}
