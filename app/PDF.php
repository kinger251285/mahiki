<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PDF extends Model
{
    public function pageSectionsPDF()
    {
        return $this->belongsToMany('App\PageSections', 'pagesections_pdf_pivot','pdf_id', 'page_section_id')->withTimestamps();
    }

    public function seasonalsPDF()
    {
        return $this->belongsToMany('App\SeasonalEvents', 'seasonals_pdf_pivot','pdf_id', 'seasonal_event_id')->withTimestamps();
    }
}
