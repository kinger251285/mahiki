<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsBanner extends Model
{
    public function Events()
    {
        return $this->belongsTo('App\Events', 'Events_id');

    }

    public function BannerMedia()
    {
        return $this->belongsToMany('App\Media', 'events_banners_media_pivot','events_banners_id', 'media_id')->withTimestamps();
    }
}
