<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class images extends Model
{
    public function pageHeaderSingle()
    {
        return $this->belongsToMany('App\pages', 'pages_header_images_pivot', 'image_id', 'page_id')->withTimestamps();
    }

    public function pageHeaderCarousel()
    {
        return $this->belongsToMany('App\pages', 'pages_header_images_carousel_pivot', 'image_id', 'page_id')->withTimestamps();
    }

    public function pageSectionsSingle()
    {
        return $this->belongsToMany('App\PageSections', 'page_sections_images_pivot', 'image_id', 'page_section_id')->withTimestamps();
    }

    public function pageSectionsCarousel()
    {
        return $this->belongsToMany('App\PageSections', 'page_sections_carousel_pivot', 'image_id', 'page_section_id')->withTimestamps();
    }

    public function eventsSingle()
    {
        return $this->belongsToMany('App\Events', 'event_images_pivot', 'image_id', 'event_id')->withTimestamps();
    }

    public function eventsCarousel()
    {
        return $this->belongsToMany('App\Events', 'event_carousel_pivot', 'image_id', 'event_id')->withTimestamps();
    }

    public function seasonalEventsSingle()
    {
        return $this->belongsToMany('App\SeasonalEvents', 'seasonal_events_images_pivot', 'image_id', 'seasonal_event_id')->withTimestamps();
    }

    public function seasonalEeventsCarousel()
    {
        return $this->belongsToMany('App\SeasonalEvents', 'seasonal_events_carousel_pivot', 'image_id', 'seasonal_event_id')->withTimestamps();
    }

    public function contactUsSingle()
    {
        return $this->belongsToMany('App\ContactUsContent', 'contact_us_content_images_pivot', 'image_id', 'contact_us_id')->withTimestamps();
    }

    public function contactUsCarousel()
    {
        return $this->belongsToMany('App\ContactUsContent', 'contact_us_content_carousel_pivot', 'image_id', 'contact_us_id')->withTimestamps();
    }

     public function whatsOnContentSingle()
    {
        return $this->belongsToMany('App\images', 'whatsoncontent_images_pivot', 'image_id', 'whats_on_content_id')->withTimestamps();
    }

    public function whatsOnContentCarousel()
    {
        return $this->belongsToMany('App\images', 'whatsoncontent_images_pivot', 'image_id', 'whats_on_content_id')->withTimestamps();
    }
}
