/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue'); 

import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import * as VueGoogleMaps from 'vue2-google-maps'
 
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBqJvBAqlxuaSs5xl33u_Kb-Ffzu8qW1zU',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
  },
 
  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})


Vue.use(Vuetify)
Vue.use(VueRouter)

import 'vuetify/dist/vuetify.min.css'
import Permissions from './mixins/Permissions';
Vue.mixin(Permissions);



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('admin', require('./components/Admin.vue').default);
Vue.component('index', require('./components/Index.vue').default);
Vue.component('VueVideo', require('./components/VueVideo.vue').default);

import Dashboard from './pages/Dashboard'
import ManageSupplierImages from './pages/ManageSupplierImages'
import HomepageEdit from './pages/HomepageEdit'
import Pages from './pages/Pages-editor'
import Templates from './pages/Templates'
import PagesAdd from './pages/PagesAdd'
import PagesEdit from './pages/PagesEdit'
import PagesSections from './pages/PagesSections'
import AddPagesSections from './pages/AddPagesSections'
import EditPagesSections from './pages/EditPagesSections'
import PagesMedia from './pages/PagesMedia'
import Events from './pages/Events'
import AddEvents from './pages/AddEvents'
import EditEvents from './pages/EditEvents'
import SeasonalEvents from './pages/SeasonalEvents'
import SeasonalEventsAdd from './pages/SeasonalEventsAdd'
import SeasonalEventsEdit from './pages/SeasonalEventsEdit'
import HomeContent from './pages/HomeContent'
import PageContent from './pages/PageContent'
import WhatsOnContent from './pages/WhatsOnContent'
import EventContent from './pages/EventContent'
import SeasonalEventContent from './pages/SeasonalEventContent'
import ContactUsContent from './pages/ContactUsContent'
import EnquiryForm from './pages/EnquiryForm'
import BookingForm from './pages/BookingForm'
import ContactInfo from './pages/ContactInfo'
import PagesSectionsPDF from './pages/PagesSectionsPDF'
import AddWhatsOnContent from './pages/AddWhatsOnContent'
import Users from './pages/Users'
import Roles from './pages/Roles'
import Permission from './pages/Permissions'
import MediaLibrary from './pages/MediaLibrary'
import SeasonalsPDF from './pages/SeasonalsPDF'


const routes = [
{
		path: '/',
		component: HomeContent
},
{
		path: '/whats-on',
		component: WhatsOnContent
},
{
		path: '/enquiry',
		component: EnquiryForm
},
{
		path: '/book-a-table',
		component: BookingForm
},
{
		path: '/whats-on/:slug',
		component: EventContent
},
{
		path: '/seasonal-event/:slug',
		component: SeasonalEventContent
},
{
		path: '/contact-us',
		component: ContactUsContent
},
{
		path: '/admin',
		component: Dashboard
},
{
		path: '/admin/suppliers/Manage-images/:slug',
		component: ManageSupplierImages
},

{
		path: '/admin/homepage-edit',
		component: HomepageEdit
},

{
		path: '/admin/pages',
		component: Pages
},
{
		path: '/admin/contact-info',
		component: ContactInfo
},
{
		path: '/admin/templates',
		component: Templates
},
{
		path: '/admin/seasonalevents',
		component: SeasonalEvents
},
{
		path: '/admin/seasonalevents/pdf/:id',
		component: SeasonalsPDF
},
{
		path: '/admin/seasonalevents/add',
		component: SeasonalEventsAdd
},
{
		path: '/admin/seasonalevents/edit/:id',
		component: SeasonalEventsEdit
},
{
		path: '/admin/events',
		component: Events
},
{
		path: '/admin/events/add',
		component: AddEvents
},
{
		path: '/admin/events/:id/contentAdd',
		component: AddWhatsOnContent
},
{
		path: '/admin/events/edit/:id',
		component: EditEvents
},

{
		path: '/admin/pages/add',
		component: PagesAdd
},

{
		path: '/admin/pages/edit/:slug',
		component: PagesEdit
},

{
		path: '/admin/pages/sections/:slug',
		component: PagesSections
},
{
		path: '/admin/pages/sections/:slug/add',
		component: AddPagesSections
},
{
		path: '/admin/pages/sections/:slug/edit/:id',
		component: EditPagesSections
},
{
		path: '/admin/pages/sections/:slug/pdf/:id',
		component: PagesSectionsPDF
},

{
		path: '/admin/pages/media/:slug',
		component: PagesMedia
},
{
		path: '/admin/users',
		component: Users
},
{
		path: '/admin/roles',
		component: Roles
},
{
		path: '/admin/permissions',
		component: Permission
},
{
		path: '/admin/medialibrary',
		component: MediaLibrary
},
{
		path: '/:slug',
		component: PageContent
},


];

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
var router = new VueRouter({
  routes: routes,
  mode: 'history'
});

const app = new Vue({
  el: '#app',
  router: router,
  vuetify: new Vuetify()  
});

global.vm = app;