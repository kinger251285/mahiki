Hi {{ $data['name'] }}, <br><br>

Thank you for enquiring about a private party at Mahiki London.<br><br>

Summary of the details of your booking are below: <br><br><br>

Name: {{ $data['name'] }}<br><br>
Email: {{ $data['email'] }}<br><br>
Phone Number: {{ $data['phone'] }}<br><br>
Booking Type: {{ $data['event_type'] }}<br><br>
Booking Day: {{ $data['event_day'] }}<br><br>
Booking Date: {{ $data['event_date'] }}<br><br>
Timing: {{ $data['start_time'] }}<br><br>
Number Of Guests: {{ $data['no_guests'] }}<br><br>
Budget: £{{ $data['budget'] }}K<br><br>
Additional Information: {{ $data['additional'] }}<br><br><br>

One of our dedicated team will respond to this booking enquiry within the next 48 hours. If your enquiry is of an urgent nature please call - 020 7493 9529. <br><br><br>

Many thanks <br><br>
Mahiki London