Hi {{ $data['name'] }}, <br><br>

Thank you for enquiring on our website and letting us know more about your business.<br><br>

Summary of the details you sent: <br><br><br><br>

Name: {{ $data['name'] }}<br><br>
Email: {{ $data['email'] }}<br><br>
Business Name: {{ $data['business_name'] }}<br><br>
Business Description: {{ $data['business_description'] }}<br><br><br><br>

Many thanks <br><br>
The Dowdeswell Park Market team