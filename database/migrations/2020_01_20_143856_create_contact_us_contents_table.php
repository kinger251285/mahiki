<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactUsContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_us_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contact_title');            
            $table->text('contact_excerpt')->nullable();
            $table->string('address_title');  
            $table->string('street_address'); 
            $table->string('address_2')->nullable();  
            $table->string('town')->nullable();   
            $table->string('city'); 
            $table->string('county');   
            $table->string('postcode');  
            $table->string('longitude')->nullable(); 
            $table->string('latitude')->nullable(); 
            $table->string('phone_title');  
            $table->string('phone_no'); 
            $table->string('email_title');   
            $table->string('email');   
            $table->string('media_type')->nullable();   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_us_contents');
    }
}
