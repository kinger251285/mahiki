<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('events_id')->nullable();
            $table->foreign('events_id')->references('id')->on('events');
            $table->string('banner_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_banners');
    }
}
