<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhatsoncontentCarouselPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatsoncontent_carousel_pivot', function (Blueprint $table) {
            $table->unsignedBigInteger('whats_on_content_id')->nullable();
            $table->foreign('whats_on_content_id')->references('id')->on('whats_on_contents');
            $table->unsignedBigInteger('image_id')->nullable();
            $table->foreign('image_id')->references('id')->on('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whatsoncontent_carousel_pivot');
    }
}
