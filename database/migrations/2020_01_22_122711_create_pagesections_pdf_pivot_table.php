<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesectionsPdfPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagesections_pdf_pivot', function (Blueprint $table) {
            $table->unsignedBigInteger('page_section_id')->nullable();
            $table->foreign('page_section_id')->references('id')->on('page_sections');
            $table->unsignedBigInteger('pdf_id')->nullable();
            $table->foreign('pdf_id')->references('id')->on('p_d_f_s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagesections_pdf_pivot');
    }
}
