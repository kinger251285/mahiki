<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonalsPdfPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasonals_pdf_pivot', function (Blueprint $table) {
            $table->unsignedBigInteger('seasonal_event_id')->nullable();
            $table->foreign('seasonal_event_id')->references('id')->on('seasonal_events');
            $table->unsignedBigInteger('pdf_id')->nullable();
            $table->foreign('pdf_id')->references('id')->on('p_d_f_s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasonals_pdf_pivot');
    }
}
