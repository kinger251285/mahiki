<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) { 
            $table->bigIncrements('id');
            $table->string('event_title');            
            $table->text('event_excerpt')->nullable();
            $table->string('event_slug')->unique();
            $table->string('event_day');
            $table->date('event_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->string('event_main_image');
            $table->unsignedBigInteger('promoter')->nullable();
            $table->foreign('promoter')->references('id')->on('users');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
