<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhatsOnBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whats_on_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('whats_on_contents_id')->nullable();
            $table->foreign('whats_on_contents_id')->references('id')->on('whats_on_contents');
            $table->string('banner_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whats_on_banners');
    }
}
