<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonalEventsBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasonal_events_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('seasonal_events_id')->nullable();
            $table->foreign('seasonal_events_id')->references('id')->on('seasonal_events');
            $table->string('banner_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasonal_events_banners');
    }
}
