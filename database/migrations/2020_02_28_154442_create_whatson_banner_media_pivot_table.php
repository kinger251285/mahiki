<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhatsonBannerMediaPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatson_banner_media_pivot', function (Blueprint $table) {
            $table->unsignedBigInteger('whats_on_banners_id')->nullable();
            $table->foreign('whats_on_banners_id')->references('id')->on('whats_on_banners');
            $table->unsignedBigInteger('media_id')->nullable();
            $table->foreign('media_id')->references('id')->on('media');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whatson_banner_media_pivot');
    }
}
