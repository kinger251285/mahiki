<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('room_name');  
            $table->string('room_floor');          
            $table->text('room_excerpt')->nullable();
            $table->string('enquiry_email')->nullable();            
            $table->string('room_image');
            $table->unsignedBigInteger('room_contact')->nullable();
            $table->foreign('room_contact')->references('id')->on('users');
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('events');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_rooms');
    }
}
