<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('currentUser', 'UsersController@currentUser');

Route::get('users/permissions/{id}', 'UsersController@userPermissions');

Route::apiResource('users', 'UsersController');

Route::apiResource('roles', 'RolesController');

Route::apiResource('permissions', 'PermissionsController');

Route::apiResource('mailchimp', 'MailchimpController');

Route::get('suppliers/Edit-supplier/{slug}', 'SuppliersController@GetSupplier');

Route::get('suppliers/Manage-images/{slug}', 'SuppliersController@ManageImages');

Route::apiResource('suppliers', 'SuppliersController');

Route::post('pages/media/{slug}/AddSingleHeader', 'ImagesController@AddSingleHeader');

Route::post('pages/media/{slug}/AddCarouselImages', 'ImagesController@AddCarouselImages');

Route::post('pages/media/{slug}/DeleteSingleImage', 'ImagesController@deleteSingleImage');

Route::post('pages/media/{slug}/DeleteCarouselImage', 'ImagesController@deleteCarouselImage');

Route::delete('suppliers/Manage-images/{slug}/DeleteImage', 'ImagesController@destroy');

Route::apiResource('images', 'ImagesController');

Route::apiResource('mailchimp', 'MailchimpController');

Route::post('booking-email', 'EmailController@SendBookingEmail');

Route::post('party-email', 'EmailController@SendPartyEmail');

Route::apiResource('templates', 'TemplatesController');

Route::get('pages/edit/{slug}', 'PagesController@GetPage');

Route::get('pages/media/{slug}', 'PagesController@GetPageHeaderImages');

Route::post('pages/media/{slug}/AddSingleHeader', 'ImagesController@AddSingleHeader');

Route::post('pages/media/{slug}/SubmitHeaderChange', 'PagesController@SubmitHeaderChange');

Route::get('pages/{slug}', 'PagesController@GetFullPage');

Route::apiResource('pages', 'PagesController');

Route::apiResource('PageSectionsBanner', 'PageSectionsBannerController');

Route::post('pageSections/{slug}/AddSingleImage', 'ImagesController@AddSectionSingleImage');

Route::post('pageSections/{slug}/AddSectionCarouselImages', 'ImagesController@AddSectionCarouselImages');

Route::post('pageSections/{slug}/EditSingleImage', 'ImagesController@EditSectionSingleImage');

Route::post('pageSections/{slug}/EditSectionCarouselImages', 'ImagesController@EditSectionCarouselImages');

Route::post('pageSections/{slug}/DeleteSingleImage', 'ImagesController@deleteSectionSingleImage');

Route::post('pageSections/{slug}/DeleteCarouselImage', 'ImagesController@deleteSectionCarouselImage');

Route::get('pageSections/{slug}', 'PageSectionsController@index');

Route::get('pageSections/getsections/{slug}', 'PageSectionsController@GetFullPageSections');

Route::get('pageSections/edit/{slug}/{id}', 'PageSectionsController@GetSection');

Route::post('pageSections/{slug}/SubmitMediaChange', 'PageSectionsController@SubmitMediaChange');

Route::apiResource('pageSections', 'PageSectionsController');

Route::post('events/AddEventSingleImage', 'EventsController@AddEventSingleImage');

Route::post('events/AddEventCarouselImages', 'EventsController@AddEventCarouselImages');

Route::post('events/EditEventSingleImage', 'EventsController@EditEventSingleImage');

Route::post('events/EditEventCarouselImages', 'EventsController@EditEventCarouselImages');

Route::get('events/edit/{id}', 'EventsController@GetEvent');

Route::post('events/DeleteMainImage/{id}', 'EventsController@DeleteMainImage');

Route::post('events/DeleteSingleImage/{id}', 'EventsController@DeleteSingleImage');

Route::post('events/DeleteCarouselImage/{id}', 'EventsController@DeleteCarouselImage');

Route::post('events/{slug}/SubmitMediaChange', 'EventsController@SubmitMediaChange');

Route::get('events/eventsummary', 'EventsController@EventSummary');

Route::get('events/Content/{slug}', 'EventsController@GetEventContent');

Route::apiResource('events', 'EventsController');

Route::post('seasonalevents/DeleteSingleImage/{id}', 'SeasonalEventsController@DeleteSingleImage');

Route::post('seasonalevents/DeleteCarouselImage/{id}', 'SeasonalEventsController@DeleteCarouselImage');

Route::get('seasonalevents/edit/{id}', 'SeasonalEventsController@GetSeasonal');

Route::get('seasonalevents/get/{id}', 'SeasonalEventsController@GetSeasonalPDF');

Route::get('seasonalevents/seasonalsummary', 'SeasonalEventsController@SeasonalSummary');

Route::get('seasonalevents/seasonaldisplay/{slug}', 'SeasonalEventsController@SeasonalDisplay');

Route::post('seasonalevents-EditSingle/{id}', 'SeasonalEventsController@EditSeasonalSingleImage');

Route::post('seasonalevents-EditCarousel/{id}', 'SeasonalEventsController@EditSeasonalCarouselImages');

Route::post('seasonalevents-AddSingle', 'SeasonalEventsController@AddSeasonalSingleImage');

Route::post('seasonalevents-AddCarousel', 'SeasonalEventsController@AddSeasonalCarouselImages');

Route::apiResource('seasonalevents', 'SeasonalEventsController');

Route::get('instagrams', 'SocialController@instagram');

Route::apiResource('socials', 'SocialController');

Route::post('contacts-edit/{id}', 'ContactUsContentController@update');

Route::post('contacts-AddSingle', 'ContactUsContentController@AddContactsSingleImage');

Route::post('contacts-AddCarousel', 'ContactUsContentController@AddContactsCarouselImages');

Route::apiResource('contacts', 'ContactUsContentController');

Route::post('PDF/seasonal/add', 'PDFController@SeasonalStore');

Route::post('PDF/seasonal/delete/{id}', 'PDFController@SeasonalDelete');

Route::apiResource('PDF', 'PDFController');

Route::post('WhatsOnContent/AddEventContentSingleImage', 'WhatsOnContentController@AddEventSingleImage');

Route::post('WhatsOnContent/AddEventContentCarouselImages', 'WhatsOnContentController@AddEventCarouselImages');

Route::get('WhatsOnContent/{slug}', 'WhatsOnContentController@index');

Route::apiResource('WhatsOnContent', 'WhatsOnContentController');

Route::get('PageBanner/GetBanner/{slug}', 'PageBannerController@GetPagesBanner');

Route::apiResource('PageBanner', 'PageBannerController');

Route::get('EventsBanner/GetBanner/{id}', 'EventsBannerController@GetEventsBanner');

Route::apiResource('EventsBanner', 'EventsBannerController');

Route::apiResource('media', 'MediaController');

Route::get('seasonalbanners/GetBanner/{id}', 'SeasonalEventsBannerController@GetEventsBanner');

Route::apiResource('seasonalbanners', 'SeasonalEventsBannerController');

Route::get('PageSectionsBanner/GetThisBanner/{id}', 'PageSectionsBannerController@GetThisBanner');

Route::apiResource('PageSectionsBanner', 'PageSectionsBannerController');


 